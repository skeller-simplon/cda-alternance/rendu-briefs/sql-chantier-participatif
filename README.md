## Sql chantier participatif


    - init_db.sql : fichiers relatifs aux différentes bases, nécessaires pour la requête.
        /!\ cette commande supprime la table 'sql_chantier_participatif' avant de la re-créer.
    - insert_datas.sql : Vide la base de donnée et créé des pseudos datas pour remplir les différentes tables.

![Capture d'écran base de donnée](database.png "Title")