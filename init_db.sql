-- Hard reset
-- A commenter en prod (nojoke).

\! echo "Dropping database 'sql_chantier_participatif'.";
DROP DATABASE IF EXISTS sql_chantier_participatif;

\! echo "Creating database 'sql_chantier_participatif'.";
CREATE DATABASE sql_chantier_participatif;

USE sql_chantier_participatif;

-- ----------------------
-- CREATION TABLE USER --
-- ----------------------

\! echo "Creating table user.";

CREATE TABLE `user`
(
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `username` varchar(50) NOT NULL,
    `password` varchar(100) NOT NULL,
    `venue_id` INT -- FK venue
);

-- -----------------------
-- CREATION TABLE VENUE --
-- -----------------------

\! echo "Creating table venue.";

CREATE TABLE `venue` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `address` VARCHAR(200) NOT NULL,
  `capacity` INT NOT NULL,
  `type` TEXT NULL);

\! echo "Creating FK user <-> user.";

-- FK VENUE - USER
ALTER TABLE `user`
    ADD CONSTRAINT FK_user_venue FOREIGN KEY(`venue_id`) 
    REFERENCES `venue`(`id`);
    
-- -----------------------
-- CREATION TABLE SKILL --
-- -----------------------

\! echo "Creating table skill.";

CREATE TABLE `skill`
(
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `level` INT,
    `user_id` INT, -- FK USER 
    `venue_activity_id` INT -- FK VENUE_ACTIVITY
);

\! echo "Creating FK skill <-> user.";

-- FK skill - USER
ALTER TABLE `skill`
    ADD CONSTRAINT `FK_user_skill` FOREIGN KEY(`user_id`) 
    REFERENCES `user`(`id`);

-- --------------------------------
-- CREATION TABLE venue activity --
-- --------------------------------

\! echo "Creating table venue_activity.";

CREATE TABLE `venue_activity` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `label` VARCHAR(100) NOT NULL
);

\! echo "Creating FK skill <-> venue_activity.";

-- FK skill - venue_activity
ALTER TABLE `skill`
    ADD CONSTRAINT `FK_skill_venue_activity` FOREIGN KEY(`venue_activity_id`) 
    REFERENCES `venue_activity`(`id`);

-- ----------------------------------------------------
-- CREATION TABLE DE JONCTION venue_venue_activity --
-- ----------------------------------------------------

\! echo "Creating junction table venue_venue_activity.";

CREATE TABLE `venue_venue_activity` (
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `venue_id` INT, -- FK project
    `venue_activity_id` INT -- FK project
);

\! echo "Creating junction FK venue <-> venue_activity.";

-- FK project - venue activities
ALTER TABLE `venue_venue_activity`
    ADD CONSTRAINT `FK_venue_venue_activity` FOREIGN KEY(`venue_id`) 
    REFERENCES `venue`(`id`);

-- FK venue activities - project
ALTER TABLE `venue_venue_activity`
    ADD CONSTRAINT `FK_project_venue_activity_venue` FOREIGN KEY(`venue_activity_id`) 
    REFERENCES `venue_activity`(`id`);

-- -------------------------
-- CREATION TABLE project --
-- -------------------------

\! echo "Creating table project.";

CREATE TABLE `project` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  `capacity` INT NOT NULL,
  `venue_id` INT -- FK venue 
);

\! echo "Creating FK project <-> venue.";

-- FK project - venue
ALTER TABLE `project`
    ADD CONSTRAINT `FK_project_venue` FOREIGN KEY(`venue_id`) 
    REFERENCES `venue`(`id`);

-- ----------------------------------------------------
-- CREATION TABLE DE JONCTION project_venue_activity --
-- ----------------------------------------------------

\! echo "Creating junction table project_venue_activity.";

CREATE TABLE `project_venue_activity` (
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `project_id` INT, -- FK project
    `venue_activity_id` INT -- FK project
);

\! echo "Creating junction FK project <-> venue_activity.";

-- FK project - venue activities
ALTER TABLE `project_venue_activity`
    ADD CONSTRAINT `FK_project_venue_activity_project` FOREIGN KEY(`project_id`) 
    REFERENCES `project`(`id`);

-- FK venue activities - project
ALTER TABLE `project_venue_activity`
    ADD CONSTRAINT `FK_project_venue_activity_venue_activity` FOREIGN KEY(`venue_activity_id`) 
    REFERENCES `venue_activity`(`id`);

-- -------------------------
-- CREATION TABLE project --
-- -------------------------

\! echo "Creating table application.";

CREATE TABLE `application` (
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `start_date` DATE,
    `end_date` DATE,
    `is_accepted` BIT,
    `project_id` INT NULL, -- FK project
    `user_id` INT, -- FK user
    `venue_id` INT
);

\! echo "Creating FK application <-> project.";

-- FK application-project
ALTER TABLE `application`
    ADD CONSTRAINT `FK_project_application` FOREIGN KEY(`project_id`) 
    REFERENCES `project`(`id`);

\! echo "Creating FK application <-> user.";

-- FK application-user
ALTER TABLE `application`
    ADD CONSTRAINT `FK_user_application` FOREIGN KEY(`user_id`) 
    REFERENCES `user`(`id`);

\! echo "Creating FK application <-> venue.";

-- FK application-venue
ALTER TABLE `application`
    ADD CONSTRAINT `FK_application_venue` FOREIGN KEY(`venue_id`) 
    REFERENCES `venue`(`id`);

-- ----------------------------
-- CREATION TABLE attendance --
-- ----------------------------

\! echo "Creating table attendance.";

CREATE TABLE `attendance`(
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `date` DATE,
    `project_id` INT, -- FK project
    `user_id` INT -- FK User
);

\! echo "Creating FK attendance <-> project.";

-- FK attendance-project
ALTER TABLE `attendance`
    ADD CONSTRAINT `FK_project_attendance` FOREIGN KEY(`project_id`) 
    REFERENCES `project`(`id`);

\! echo "Creating FK attendance <-> user.";

-- FK attendance-user
ALTER TABLE `attendance`
    ADD CONSTRAINT `FK_user_attendance` FOREIGN KEY(`user_id`) 
    REFERENCES `user`(`id`);

    