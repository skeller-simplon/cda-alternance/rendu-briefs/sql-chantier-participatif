CREATE DEFINER=`skeller`@`localhost` PROCEDURE `validate_attendance`(
	IN idUser nvarchar(5),
    IN idProject nvarchar(5)
)
BEGIN
	-- Valeur à incrémenter pour les skills
	DECLARE incrementValue INT DEFAULT 50;
    -- Prérequis pour une loop
    DECLARE increment INT DEFAULT 0;
    DECLARE rowCount INT DEFAULT 0;
    DECLARE updatedCount INT DEFAULT 0;

	-- Ids des activités d'un projet.  
	CREATE TEMPORARY TABLE project_activity_ids (id int); 
	INSERT INTO project_activity_ids 
		SELECT venue_activity_id
        FROM project_venue_activity
		WHERE project_id = idProject;
	
    -- Le user était bien présent, on ajoute une ligne
	INSERT INTO `attendance` (`date`, `project_id`, `user_id`) VALUES (CURDATE(), idProject, idUser);
    
	-- Le count donne le nombre de loops;
	SELECT COUNT(*) FROM project_activity_ids INTO rowCount;
	WHILE increment < rowCount DO
		-- Essaye de mettre à jour un skill en utilisant l'incrément.
		UPDATE `skill`
			SET `level` = `level` + incrementValue
			WHERE `user_id` = idUser AND `venue_activity_id` = (SELECT (id) FROM project_activity_ids ORDER BY ID LIMIT 1 OFFSET increment);

		-- Si aucune row n'a été mise à jour => On créé.
		SELECT ROW_COUNT() INTO updatedCount;
		IF updatedCount = 0 THEN
			INSERT INTO `skill` (`level`, `user_id`, `venue_activity_id`) VALUES 
			(incrementValue, idUser, (SELECT (id) FROM project_activity_ids ORDER BY ID LIMIT 1 OFFSET increment));
		END IF;
        SET increment = increment + 1;
	END WHILE;
	
    DROP TABLE project_activity_ids;
END